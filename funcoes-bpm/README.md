# Funções JS para BPM

## Observação

Foi criado um Funções oficial (pasta funcoes-oficial) que deve ser usado para manter um padrão entre os clientes, então **só use esse Funções como base para algo que não tenha no oficial**. Auxilios, melhorias e manutenções a cargo do Marcos Santos (live:marcos.santos_6)

## Resumo

Funções uteis para desenvolvimento de processos, incluido váriaveis de ambiente, funções que funcionam com **regra de tela** e funções sómente para uso via **js**

## Conteúdo do Funções

### VARIÁVEIS DE AMBIENTE

---

- **FORM_COD** --> Código do formulário
- **FORM_VERSAO** --> Versão do formulário
- **FORM_PUBLICO** --> Se o formulário é público ou não
- **PROCESSO_COD** --> Código do processo atual
- **ETAPA_TITULO** --> Titulo da etapa atual que aparece no cabeçalho do formulário
- **ETAPA_SUB_TITULO** --> Sub titulo da etapa atual que aparece no cabeçalho do formulário
- **ETAPA_COD** --> Código da etapa atual
- **ETAPA_CICLO** --> Número do ciclo atual da etapa
- **ETAPA_DATA_INICIO** --> Data que a etapa foi iniciada (Mesmo formato que o campo do tipo data)
- **ETAPA_DATA_PRAZO** --> Data que a etapa fica atrasada (Mesmo formato que o campo do tipo data)
- **ETAPA_STATUS** --> Estatus da etapa atual se for A, o processo esta em andamento
- **DOCUMENTO_STORE** --> Manipulação de anexos
- **HOJE** --> Objeto Date contendo somente a data atual (sem hora, minuto, segundo)

---

### FUNÇÕES JS / REGRA DE TELA

---

- **validacaoEmail** --> Verfica se o email informado possui um padrão válido e insere um erro no campo caso seja necessário.
- **validaCPF** --> Verifica se o CPF do campo informado é valido.
- **validaCNPJ** --> Verifica se o CNPJ do campo informado é valido.
- **validaPIS** --> Valida se o PIS informado é valido.
- **limpaFormulario** --> Remove os valores de todos os campos e grids do formulário.
- **modal** --> Modal simples do bpm com apenas um botão de confirmação.

---

### FUNÇÕES SÓMENTE PARA JS

---

- **getCampos** --> Simplificação Form.fields(), Form.grids().fields().
- **getValorCampo** --> Coleta valor do campo evitando retorno undefined ou de um array.
- **setValorCampo** --> Insere um valor em um campo.
- **setCamposVisivel** --> Simplificação para configurar campo visivel ou invisivel. Obs: já remove obrigatoridedade caso o campo seja configurado como invisivel.
- **setCamposObrigatorio** --> Simplificação para configurar campo visivel ou invisivel. Obs: já configura o campo como visivel caso o campo seja obrigatório.
- **setCamposLeitura** --> Simplificação para configurar campo leitura ou normal. Obs: já remove obrigatoridedade caso o campo seja configurado como leitura.
- **setCamposBloqueado** --> Simplificação para configurar campo bloqueado ou normal.
- **campoExiste** --> Verifica se o campo está disponivel na etapa para ser manipulado.
- **getCamposTipo** --> Retorna em um array os objetos dos campos com o tipo informado.
- **criarData** --> Cria um objeto do tipo data do valor informado.
- **criaDataString** --> Converte uma data para o padrão desejado.
- **insereErroCampo** --> Adiciona ou remove uma mensagem de erro ao campo passado como parametro.
- **validaGridVazia** --> Valida se uma grid possui valores inseridos nela.

*Mais informações nos comentários das funções

## Exemplo de utilização funções compativeis com Regra de tela

Para configurar uma regra de tela com uma função basta basta escolher o evento que irá chamar a função e em "execute as ações" escolher "Definir Valor", definir o campo que será manipulado, escolher o valor "Operação" e passar a função com o nome do campo como parametro.

**Observação**: Execução de js via regra de tela **Não** funciona com o evento "Valores dos campos carregados com sucesso".


![Exemplo validaCPF](./imagens/exemplo_regra_de_tela.png)
