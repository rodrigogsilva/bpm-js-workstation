// ==UserScript==
// @name            Funcoes Auxiliares
// @version         1.0
// @description     Funções para auxiliar no desenvolvimento de processos
// @author          Rodrigo Silva
// @include         http://*/*
// @include         https://*/*
// @grant           none
// ==/UserScript==

// Form App
window.bpm_campo = nomeCampo => {
    nomeCampo = (nomeCampo || '').toString().trim();
    const todosCampos = Form.allFields();

    const retorno = (!!nomeCampo) ? todosCampos.find(campo => campo.id === nomeCampo) : todosCampos;

    if (!!retorno) return retorno;

    throw new Error('Campo ' + nomeCampo + ' não foi encontrado na Etapa.');
}

window.bpm_valor = nomeCampo => {
    if (!nomeCampo)
        throw new Error('Por favor informe qual campo deve ser manipulado.');

    return bpm_campo(nomeCampo).value();
}

window.bpm_mostrarCampos = () => {
    bpm_campo(nomeCampo).forEach(campo => campo.visible(true));
    Form.apply();
}

window.bpm_clonarEtapa = () => JSON.stringify(Form.fields().filter(campo => !!campo.value()).map(campo => ({
    GRID: false,
    ID: campo.id,
    VAL: campo.value().toString()
})).concat(Form.grids().map(grid => {
    return {
        GRID: true,
        ID: grid.id,
        VAL: grid.dataRows()
    }
})));

window.bpm_preencherEtapa = valores => {
    valores.forEach(param => {
        if (param.GRID) {
            if (Form.grids(param.ID))
                param.VAL.forEach(val => Form.grids(param.ID).insertDataRow(val));
        } else {
            if (Form.fields(param.ID))
                Form.fields(param.ID).value(param.VAL);
        }
    });

    Form.apply();
}

// Studio
window.bpm_getApresentacaoCampo = () =>
    Array.from($('.presentation select [selected="selected"')).map(v => v.value);

window.bpm_setApresentacaoCampo = (vals) => {
    vals.forEach((val, index) => {
        const idx = index + 1;
        $('#apresentacao_' + (idx)).val(val);
        selecionarObrigatorio($('#apresentacao_' + (idx))[0], (idx), 'true', false);
    });
}

window.bpm_clonarConfigEtapa = () => JSON.stringify(Array.from($('.presentation')).reduce((all, _, index) => {
    const idx = index + 1
    if (!!$('#apresentacao_' + (idx)).val())
        all.push({
            VAL: $('#apresentacao_' + (idx)).val(),
            APROVA: $('#obriga_aprova_' + (idx)).is(':checked'),
            REPROVA: $('#obriga_reprova_' + (idx)).is(':checked'),
            INICIALIZACAO: $('#inicializar_' + (idx)).val(),
            INIVAL: $('#valor_' + (idx)).val(),
            CICLICO: $('#executaCiclico_' + (idx)).is(':checked'),
            MANTERVALOR: $('#manter_valor_lista_' + (idx)).is(':checked'),
        });
    return all;
}, []));

window.bpm_preencherConfigEtapa = valores => valores.forEach((param, index) => {
    const idx = index + 1

    $('#apresentacao_' + (idx)).val(param.VAL)
    selecionarObrigatorio($('#apresentacao_' + (idx))[0], (idx), 'true', false);

    if (param.VAL == 'O' || param.VAL == 'M') {
        if (!$('#obriga_aprova_' + (idx)).is(':checked') !== param.APROVA)
            $('#obriga_aprova_' + (idx)).click();

        if ($('#obriga_reprova_' + (idx)).is(':checked') !== param.REPROVA)
            $('#obriga_reprova_' + (idx)).click();
    }

    $('#inicializar_' + (idx)).val(param.INICIALIZACAO);
    habilitaPropriedadesAdicionais((idx), param.INICIALIZACAO, 'S');

    $('#valor_' + (idx)).val(param.INIVAL);

    if ($('#manter_valor_lista_' + (idx)).is(':checked') !== param.MANTERVALOR)
        $('#manter_valor_lista_' + (idx)).click();

    if ($('#executaCiclico_' + (idx)).is(':checked') !== param.CICLICO)
        $('#executaCiclico_' + (idx)).click();
});

/**
 * @param {String} action Qual o valor incial => S == Sem valor inicial,
 * C == Com valor inicial, R == Resultado de select,
 * J == Resultado de integração
 * @param {String} valor valor que será preenchido no campo valor inicial
 * @param {Boolean} ciclico (Opicional) se o campo será ciclico
 * @param {Boolean} manterValor (Opicional) se o campo deve manter o valor em caso de lista
 */
window.bpm_preencherValorInicial = (action, valor, ciclico, manterValor) => {
    ciclico = !!ciclico;
    manterValor = !!manterValor;

    Array.from($('.initialization')).forEach((_, index) => {
        const idx = index + 1;
        $("#inicializar_" + idx).val(action);
        habilitaPropriedadesAdicionais(idx, action, 'S');
        $("#valor_" + idx).val(valor);
        $("#manter_valor_lista_" + idx).prop('checked', manterValor);
        $("#executaCiclico_" + idx).prop('checked', ciclico);
    });
}