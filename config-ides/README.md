# Configurações de IDEs

## VS Code

## Resumo

Configurações do Visual Studio Code para facilitar o desenvolvimento de js

## Extensões

- **Beautify** --> Melhora a aparência do código e cria um certo "padrão" para o estilo de código
- **Bracket Pair Colorizer** --> Colore os parenteses, chaves e colchetes de acordo com a hierarquia (parenteses dentro de parenteses. Pesquisa a extensão que é mais fácil de entender)
- **ESLint** --> Verifica erros no código js (não é perfeito mas ajuda quando falta fechar um parenteses ou algo do tipo)
- **GitLens — Git supercharged** --> Indica na linha do código quem aleterou a linha (util para dar suporte)
- **indent-rainbow** --> Muda a cor da indentação para facilitar saber onde uma função fecha
- **Settings Sync** --> Cria um perfil no gist (github) contendo suas configurações do vscode

## Atalhos

- **ctrl+k ctrl+s** --> Abre atalhos do VS Code para pesquisa ou alteração
- **ctrl+,** --> Abre configurações do VS Code
- **ctrl+g** --> Abre tela para pesquisar linha do arquivo atual
- **Ctrl+K M** --> Escolhe linguagem do arquivo
- **ctrl+p** --> Janela para Abrir Arquivos do projeto atual
- **ctrl+shift+p** --> Janela para pesquisar configurações do vscode
- **ctrl+shift+f** --> Formata o arquivo atual (utiliza o beautify se estiver configurado)
- **ctrl+shift+d** --> Copia linha atual do curso abaixo
- **ctrl+d** --> Deleta linha onde o cursor está
- **ctrl+espaço** --> auto completar (depende da linguagem do arquivo)
- **shift+alt+uparrow(seta para cima)** --> Insere um cursor na linha de cima
- **shift+alt+downarrow(seta para baixo)** --> Insere um cursor na linha de baixo
- **ctrl+numpad_divide(ou /)** --> Comenta ou descomenta tudo que for selecionado
- **ctrl+shift+c l** --> Converte texto selecionado para minuscula
- **ctrl+shift+c u** --> Converte texto selecionado para maiuscula
- **ctrl+shift+c c** --> Converte texto selecionado para Titulo (Primeira Letra De Cada Palavra Em Maiusculo)

## Snippets

### O que são

Snippets são auto completes com códigos prontos para facilitar no desenvolvimento (faz tempo que não uso por isso tem pouco)

### Configuração

- **ctrl+shift+p**
- Pesquise **Preferences: Configure User Snippet** e precione Enter
- Pesquise **javascript.json** e precione Enter
- Cole o conteudo do arquivo Snippet.json e salve
- Pronto! Agora todo arquivo js possui os comandos abaixo

### Comandos

- **bpminit** --> Template padrão para iniciar um js.
- **bpmvariaveis** --> Variáveis de ambiente do BPM
- **bpmgroup** --> Manipulação de agrupadores (grupos) do Form App
- **bpmfield** --> Manipulação de Campos (fields) do Form App
- **bpmgrid** --> Manipulação de grids do Form App
- **bpmgridvals** --> Retorna array com as linhas da grid.
- **bpmaction** --> Actions de um campo com lupa, refresh etc
- **bpmListAdd** --> Adicionar elementos a um lista do BPM.
- **bpmevent** --> Cria um event listenner no campo do formulário.
- **bpmeventlupe** --> Cria um event listenner no campo do tipo lupa no formulário.
- **bpmeventgrid** --> Eventos executados dentro de grids
- **bpmgridclear** --> Remove todos os valores de uma grid.
- **bpmmask** --> Adicionar uma mascara ao campo.
- **bpmmodal** --> modal 5.30
- **bpmloader** --> Loader do Form App
- **bpmajax** --> código para requisições ajax

## Eclipse

## Resumo

Configurações do Eclipse para facilitar o desenvolvimento de java

## Extensões

- **EGit** --> Integração do eclipse com git

## Atalhos
// TODO - finalizar
- **ctrl+k ctrl+s** --> Abre atalhos do VS Code para pesquisa ou alteração
- **ctrl+,** --> Abre configurações do VS Code
- **ctrl+g** --> Abre tela para pesquisar linha do arquivo atual
- **Ctrl+K M** --> Escolhe linguagem do arquivo
- **ctrl+p** --> Janela para Abrir Arquivos do projeto atual
- **ctrl+shift+p** --> Janela para pesquisar configurações do vscode
- **ctrl+shift+f** --> Formata o arquivo atual (utiliza o beautify se estiver configurado)
- **ctrl+shift+d** --> Copia linha atual do curso abaixo
- **ctrl+d** --> Deleta linha onde o cursor está
- **ctrl+espaço** --> auto completar (depende da linguagem do arquivo)
- **shift+alt+uparrow(seta para cima)** --> Insere um cursor na linha de cima
- **shift+alt+downarrow(seta para baixo)** --> Insere um cursor na linha de baixo
- **ctrl+numpad_divide(ou /)** --> Comenta ou descomenta tudo que for selecionado
- **ctrl+shift+c l** --> Converte texto selecionado para minuscula
- **ctrl+shift+c u** --> Converte texto selecionado para maiuscula
- **ctrl+shift+c c** --> Converte texto selecionado para Titulo (Primeira Letra De Cada Palavra Em Maiusculo)

## Snippets

### O que são

Snippets são auto completes com códigos prontos para facilitar no desenvolvimento (faz tempo que não uso por isso tem pouco)

### Configuração

- **ctrl+shift+p**
- Pesquise **Preferences: Configure User Snippet** e precione Enter
- Pesquise **javascript.json** e precione Enter
- Cole o conteudo do arquivo Snippet.json e salve
- Pronto! Agora todo arquivo js possui os comandos abaixo

### Comandos

- **bpminit** --> Template padrão para iniciar um js.
- **bpmvariaveis** --> Variáveis de ambiente do BPM
- **bpmgroup** --> Manipulação de agrupadores (grupos) do Form App
- **bpmfield** --> Manipulação de Campos (fields) do Form App
- **bpmgrid** --> Manipulação de grids do Form App
- **bpmgridvals** --> Retorna array com as linhas da grid.
- **bpmaction** --> Actions de um campo com lupa, refresh etc
- **bpmListAdd** --> Adicionar elementos a um lista do BPM.
- **bpmevent** --> Cria um event listenner no campo do formulário.
- **bpmeventlupe** --> Cria um event listenner no campo do tipo lupa no formulário.
- **bpmeventgrid** --> Eventos executados dentro de grids
- **bpmgridclear** --> Remove todos os valores de uma grid.
- **bpmmask** --> Adicionar uma mascara ao campo.
- **bpmmodal** --> modal 5.30
- **bpmloader** --> Loader do Form App
- **bpmajax** --> código para requisições ajax
