# Workstation para desenvolvimento JS para BPM

## Resumo

Alguns arquivos e configurações para facilitar desenvolvimento de javascript para processos do BPM

## Conteúdo

- **codigos-uteis** -->Algumas funções relacionadas ao BPM que podem ser uteis (nem todas são para usar no js de algum processo).
- **funcoes-oficial** --> Arquivo oficial de funções para facilitar o desenvolvimento de processos.
- **funcoes-bpm** --> Arquivo de funções baseado em um funções não oficial **Somente para referência**.
- **funcoes-auxiliar** --> Arquivo de funções para para serem salvas no browser com TamperMonkey para agilizar o desenvolvimento.
- **config-ides** --> Configurações para Visual Studio Code e eclipse para facilitar desenvolvimento de processos.
