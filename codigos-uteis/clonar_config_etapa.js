// OBS: Configurações de lupa não são passadas para a etapa que receberá os valores

// NO CONSOLE DA ETAPA QUE SERÁ CLONADA
JSON.stringify(Array.from($('.presentation')).reduce((all, _, idx) => {
    if (!!$('#apresentacao_' + (idx + 1)).val())
        all.push({
            VAL: $('#apresentacao_' + (idx + 1)).val(),
            APROVA: $('#obriga_aprova_' + (idx + 1)).is(':checked'),
            REPROVA: $('#obriga_reprova_' + (idx + 1)).is(':checked'),
            INICIALIZACAO: $('#inicializacao_' + (idx + 1)).val(),
            INIVAL: $('#valor_' + (idx + 1)).val(),
            CICLICO: $('#executarCiclico_' + (idx + 1)).is(':checked'),
            MANTERVALOR: $('#manter_valor_lista_' + (idx + 1)).is(':checked'),
        })
    return all;
}, []));

// NO CONSOLE DA ETAPA QUE RECEBERÁ OS VALORES
valores = "VALORES GERADOS ACIMA SEM ASPAS";

valores.forEach((param, idx) => {
    $('#apresentacao_' + (idx + 1)).val(param.VAL)
    selecionarObrigatorio($('#apresentacao_' + (idx + 1))[0], (idx + 1), 'true', false);
    if (param.VAL == 'O' || param.VAL == 'M') {
        if (param.APROVA) {
            if (!$('#obriga_aprova_' + (idx + 1)).is(':checked'))
                $('#obriga_aprova_' + (idx + 1)).click();
        }
        if (param.REPROVA) {
            if (!$('#obriga_reprova_' + (idx + 1)).is(':checked'))
                $('#obriga_reprova_' + (idx + 1)).click();
        }
    }
    if (param.INICIALIZACAO) {
        $('#inicializacao_' + (idx + 1)).val(param.INICIALIZACAO);
        habilitaPropriedadesAdicionais((idx + 1), param.INICIALIZACAO, 'S');
        $('#valor_' + (idx + 1)).val(param.INIVAL);
    }
    if (param.MANTERVALOR) {
        if (!$('#manter_valor_lista_' + (idx + 1)).is(':checked'))
            $('#manter_valor_lista_' + (idx + 1)).click();
    }
    if (param.CICLICO) {
        if (!$('#executarCiclico_' + (idx + 1)).is(':checked'))
            $('#executarCiclico_' + (idx + 1)).click();
    }
});