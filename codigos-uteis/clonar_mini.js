// ==UserScript==
// @name         Clonar Etapas
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Funções para clonar a etapa de um processo
// @author       Rodrigo Silva
// @include        *
// @grant        none
// ==/UserScript==
window.clonarEtapa=()=>JSON.stringify(Form.fields().filter(a=>!!a.value()).map(a=>({GRID:!1,ID:a.id,VAL:a.value().toString()})).concat(Form.grids().map(a=>({GRID:!0,ID:a.id,VAL:a.dataRows()})))),window.inserirValores=a=>{a.forEach(a=>{a.GRID?Form.grids(a.ID)&&a.VAL.forEach(b=>Form.grids(a.ID).insertDataRow(b)):Form.fields(a.ID)&&Form.fields(a.ID).value(a.VAL)}),Form.apply()};