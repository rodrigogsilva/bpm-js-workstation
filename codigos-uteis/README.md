# Funções JS para BPM

## Resumo

Algumas funções relacionadas ao BPM que podem ser uteis (nem todas são para usar no js de algum processo)

## Arquivos

- **clonar_config_etapa.js** --> Função para clonar configurações de etapas (Studio>Etapas>Propriedades dos campos) de uma etapa para outra
- **clonar_etapa.js** --> Função para copiar os valores de campos e grids de uma etapa para inserir em outro processo (campos template não são linkados ao arquivo até a etapa ser aprovada)
- **configuracoes_studio.js** --> Funções para copiar parametros de um campo / etapa para outro
- **loader_mais_preciso.js** --> Função de loader com validações para evitar do loader não fechar (ainda pode ocorre o bug de referencia do produto onde o próprio bpm perde a modal do loader e não consegue fechar)
- **submodal.js** --> Forma de abrir uma modal após a outra ser fechada
