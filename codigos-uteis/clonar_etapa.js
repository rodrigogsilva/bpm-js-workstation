// OBS: após copiados, anexos não funcionaram corretamente até a etapa ser 
// aprovada

// NO CONSOLE DO PROCESSO QUE SERÁ CLONADO
JSON.stringify(Form.fields().filter(campo => !!campo.value()).map(campo => ({
    GRID: false,
    ID: campo.id,
    VAL: campo.value().toString()
})).concat(Form.grids().map(grid => {
    return {
        GRID: true,
        ID: grid.id,
        VAL: grid.dataRows()
    }
})));

// NO CONSOLE DO PROCESSO QUE RECEBERÁ OS VALORES
valores = "VALORES GERADOS ACIMA SEM ASPAS";

valores.forEach(param => {
    if (param.GRID) {
        if (Form.grids(param.ID))
            param.VAL.forEach(val => Form.grids(param.ID).insertDataRow(val));
    } else {
        if (Form.fields(param.ID))
            Form.fields(param.ID).value(param.VAL);
    }
});

Form.apply();