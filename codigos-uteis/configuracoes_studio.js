// TODO finalizar isso

// Clona as apresentações do campo por etapa

getAp = function () {
    var apre = $('.presentation select [selected="selected"');
    var apreArr = $.map(apre, (value, index) => [value]);
    var res = apreArr.map(valor => valor.value);

    return res;
}

setAp = function (vals) {
    for (let index = 1; index < vals.length + 1; index++) {
        $('#apresentacao_' + index).val(vals[index - 1]);
    }
}

/******************************************************************************/
// Clona a informação de valor inicial do campo

getIni = function () {
    var ini = $('.initialization select [selected="selected"');
    var apreArr = $.map(ini, (value, index) => [value]);
    var res = apreArr.map(valor => valor.value);

    return res;
}

setIni = function (vals) {
    for (let index = 1; index < vals.length + 1; index++) {
        $('#inicializar_' + index).val(vals[index - 1]);
        habilitaPropriedadesAdicionais(index, vals[index - 1], 'S');
    }
}

/******************************************************************************/

/**
 * @param {String} action Qual o valor incial => S == Sem valor inicial,
 * C == Com valor inicial, R == Resultado de select,
 * J == Resultado de integração 
 * @param {String} valor valor que será preenchido no campo valor inicial
 * @param {Boolean} ciclico se o campo será ciclico
 * @param {Boolean} manterValor se o campo deve manter o valor em caso de lupa
 */
preencherVal = function (action, query, ciclico, manterValor) {
    const count = $('.initialization').length;
    // Marca ou desmarca os checkbox 
    ciclico = !!ciclico;
    manterValor = !!manterValor;

    for (let i = 1; i <= count; i++) {
        $("#inicializar_" + i).val(action);
        habilitaPropriedadesAdicionais(i, action, 'S');

        $("#valor_" + i).val(query);
        // $("#manter_valor_lista_"+i).prop('checked', manterValor);
        $("#executaCiclico_" + i).prop('checked', ciclico);
    }
}