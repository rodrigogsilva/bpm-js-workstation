let STOP_LOAD = false;

/**
 * Evita que o loader possar ficar aberto por um problema de sincronismo
 * Obs1: Depende de uma variavel global STOP_LOAD que informa a função
 * quando parar
 * Obs2: Ainda pode ficar aberto eternamente por um bug do produto que perde a 
 * referência do loader
 */
const controlaLoader = function () {
    Form.showLoader();
    // existe um pequeno delay entre o loader aparecer e o bpm entender isso
    // então o timeout se torna necessário
    setTimeout(function () {
        const intervalo = setInterval(function () {
            if (STOP_LOAD && $('#custom-modal-api').is(':visible')) {
                Form.hideLoader();
                STOP_LOAD = false;
                clearInterval(intervalo);
            }
        }, 300);
    }, 300);
}