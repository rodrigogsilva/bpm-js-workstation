let ERROR = '';

Form.addCustomModal({
    title: 'TITULO',
    description: 'MENSAGEM',
    showButtonClose: true,
    buttons: [{
        name: 'OK',
        icon: 'check',
        closeOnClick: true,
        action: function () {
            /** FAZ ALGUMA COISA AQUI*/

            // só para mudar o resultado final aleatóriamente
            ERROR = Math.random() * 100 > 50 ? 'Algum erro' : '';

            esperaModalFechar();
        }
    }]
});

/**
 * Aguarda modal de confirmação finalizar para abrir outra modal
 */
const esperaModalFechar = function () {
    const intervalo = setInterval(function () {
        if (!$('#custom-modal-api').is(':visible')) {
            if (ERROR) {
                console.log(ERROR);
                modalBPM('ERRO', ERROR);
            } else {
                console.log('SUCESSO!');
                modalBPM('SUCESSO!', 'SUCESSO!');
            }
            clearInterval(intervalo);
        }
    }, 300);
}

/**
 * Modal para apresentação de mensagens
 * @param {String} title Titulo da modal
 * @param {String} message Mensagem a ser exibida
 */
const modalBPM = function (title, message) {
    Form.addCustomModal({
        title: title,
        description: message,
        showButtonClose: true,
        buttons: []
    });
}